require("dotenv").config();

const env = (key, defaultValue = null) => process.env[key] || defaultValue;

const config = {
  app: {
    env: env("NODE_ENV"),
    url: env("APP_URL", "http://localhost:3000"),
    corsSites: env("APP_CORS_SITES", "http://localhost:3000"),
    port: parseInt(env("PORT", 3000)),
    host: env("APP_HOST", "127.0.0.1"),
    frontendUrl: env("APP_FRONTEND_URL"),
    keyPath: env("KEY_PATH"),
    certPath: env("CERT_PATH"),
    caPath: env("CA_PATH"),
  },
  db: {
    url: env("DATABASE_URL"),
  },
  auth: {
    secretKey: env("SECRET_KEY", "I8T27fsMIeLGiiaI0sU7RPLZYWVwZKSE"),
    expiresIn: env("EXPIRES_IN", 2629800000),
  },
};

module.exports = config;
