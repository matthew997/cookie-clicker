const HttpStatuses = require("http-status-codes");
const progresRepository = require("../repositories/ProgresRepository");
const config = require("../config");

class ProgressController {
  async show(req, res) {
    const { id: userId } = req.loggedUser;
    const progres = await progresRepository.findOne({ where: { userId } });

    if (!progres) {
      return res.status(HttpStatuses.NOT_FOUND);
    }

    return res.status(HttpStatuses.OK).send(progres);
  }

  async update(req, res) {
    const { id: userId } = req.loggedUser;
    const progres = await progresRepository.findOne({ where: { userId } });

    if (!progres) {
      return res.status(HttpStatuses.NOT_FOUND);
    }

    const updatedProgress = await progres.update({ ...req.body });

    return res.status(HttpStatuses.OK).send(updatedProgress);
  }
}

module.exports = new ProgressController();
