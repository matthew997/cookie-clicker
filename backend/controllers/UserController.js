const HttpStatuses = require("http-status-codes");
const jwt = require("jsonwebtoken");
const config = require("../config");
const userRepository = require("../repositories/UserRepository");
const progresRepository = require("../repositories/ProgresRepository");

class UserController {
  async login(req, res) {
    const { nickname } = req.body;
    const { secretKey, expiresIn } = config.auth;

    const user = await userRepository.findOne({ where: { nickname } });

    if (!user) {
      return res.sendStatus(HttpStatuses.UNAUTHORIZED);
    }

    const { id } = user;

    const JWTtoken = jwt.sign({ id }, secretKey, {
      expiresIn,
    });

    return res.send({
      token: JWTtoken,
      expiresIn,
      user,
    });
  }

  async create(req, res) {
    const { nickname } = req.body;

    const user = await userRepository.findOne({
      where: {
        nickname,
      },
    });

    if (user) {
      return res.status(HttpStatuses.CREATED).send(user);
    }

    const createdUser = await userRepository.create({ ...req.body });

    if (!createdUser) {
      return res.status(HttpStatuses.EXPECTATION_FAILED);
    }

    const { id: userId } = createdUser;

    const progres = await progresRepository.findOne({
      where: { userId },
    });

    if (!progres) {
      await progresRepository.create({ userId });
    }

    return res.status(HttpStatuses.CREATED).send(createdUser);
  }
}

module.exports = new UserController();
