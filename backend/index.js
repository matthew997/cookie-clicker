const express = require("express");
const app = express();
const config = require("./config");

require("./plugins/bodyParser")(app);

const routes = require("./routes")(app);

app.use("/api/v1", routes);

if (config.app.env === "development") {
  server = require("http").createServer(app);
} else {
  server = https.createServer(
    {
      key: fs.readFileSync(config.app.keyPath),
      cert: fs.readFileSync(config.app.certPath),
      ca: fs.readFileSync(config.app.caPath),
      requestCert: false,
      rejectUnauthorized: false,
    },
    app
  );
}

const port = process.env.PORT || 3000;
server.listen(port, () => console.log(`Working on port ${port}`));
