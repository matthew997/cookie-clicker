const { User } = require("../models");
const config = require("../config");
const jwt = require("jsonwebtoken");

const decodeToken = (token) => {
  if (!token) {
    return null;
  }

  const BEARER = "Bearer";
  const authorizationToken = token.split(" ");

  if (authorizationToken[0] !== BEARER) {
    return null;
  }

  try {
    const decoded = jwt.verify(authorizationToken[1], config.auth.secretKey);

    return decoded;
  } catch (err) {
    console.error("invalid token");

    return null;
  }
};

const isLoggedIn = async (req, res, next) => {
  const token = req.headers.authorization;

  if (token === null) {
    return false;
  }
  const decoded = decodeToken(token);

  if (decoded === null) {
    return res.status(401).send({ message: "Invalid token" });
  }

  req.loggedUser = await User.findByPk(decoded.id);

  return next();
};

module.exports = isLoggedIn;
