"use strict";
const DataTypes = require("sequelize/lib/data-types");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Progres", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      userId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      cookies: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      grandmas: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      cursors: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Progresses");
  },
};
