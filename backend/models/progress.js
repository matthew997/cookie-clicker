module.exports = (sequelize, Sequelize) => {
  const DataTypes = require("sequelize/lib/data-types");
  const Progres = sequelize.define("Progres", {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      unique: true,
    },
    userId: {
      type: Sequelize.UUID,
      allowNull: false,
    },
    cookies: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    grandmas: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    cursors: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
  });

  Progres.associate = function (db) {};

  return Progres;
};
