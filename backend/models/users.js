module.exports = (sequelize, Sequelize) => {
  const DataTypes = require("sequelize/lib/data-types");
  const User = sequelize.define("User", {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      unique: true,
    },
    nickname: {
      type: Sequelize.STRING,
    },
  });

  User.associate = function (db) {
    User.hasOne(db.Progres, { foreignKey: "userId" });
  };

  return User;
};
