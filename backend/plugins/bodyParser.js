const bodyParser = require("body-parser");
const config = require("../config");

module.exports = (app) => {
  app.use(bodyParser.urlencoded({ extended: true, limit: "1mb" }));
  app.use((req, res, next) => {
    bodyParser.json({ limit: config.app.jsonRequestSizeLimit })(req, res, next);
  });
};
