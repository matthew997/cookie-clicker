const AbstractRepository = require("./AbstractRepository");
const { Progres } = require("../models");

class ProgresRepository extends AbstractRepository {
  get model() {
    return Progres;
  }
}

module.exports = new ProgresRepository();
