const express = require("express");
const router = express.Router();
const fs = require("fs");

module.exports = (app) => {
  fs.readdirSync(__dirname).forEach((route) => {
    route = route.split(".")[0];

    if (route === "index") {
      return;
    }

    router.use(require(`./${route}`)(app));
  });

  return router;
};
