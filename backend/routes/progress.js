const express = require("express");
const router = express.Router();
const progressController = require("../controllers/ProgressController");
const { isLoggedIn } = require("../middleware");

module.exports = (app) => {
  router.get(
    "/progress",
    isLoggedIn,
    async (...args) => await progressController.update(...args)
  );

  router.patch(
    "/progress",
    isLoggedIn,
    async (...args) => await progressController.update(...args)
  );

  return router;
};
