const express = require("express");
const router = express.Router();
const { validate } = require("../middleware");
const userValidator = require("../validators/userValidator");
const userController = require("../controllers/UserController");

module.exports = (app) => {
  router.post(
    "/user",
    [userValidator.create, validate],
    async (...args) => await userController.create(...args)
  );
  router.post(
    "/login",
    [userValidator.create, validate],
    async (...args) => await userController.login(...args)
  );

  return router;
};
