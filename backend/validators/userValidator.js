const { body } = require("express-validator");

const create = [
  body(["nickname"])
    .trim()
    .not()
    .isEmpty()
    .withMessage("Should not be empty")
    .bail(),
];

module.exports = {
  create,
};
