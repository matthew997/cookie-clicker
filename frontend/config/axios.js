import axios from "axios";

if (localStorage.token) {
  axios.defaults.headers.common["Authorization"] =
    "Bearer " + JSON.parse(localStorage.token);
}

export default axios.create({
  baseURL: process.env.REACT_APP_API,
});
