import React, { useEffect } from "react";
import { useSetRecoilState } from "recoil";
import {
  cookiesCounter,
  cursorCounter,
  grandmasCounter,
  clearLocalStorage,
} from "@state/store";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import routes from "@router";

const App = () => {
  const setCookiesCounter = useSetRecoilState(cookiesCounter);
  const setCursors = useSetRecoilState(cursorCounter);
  const setGrandmas = useSetRecoilState(grandmasCounter);

  useEffect(() => {
    const { cookies, cursors, grandmas, level } = localStorage;

    if (!cookies || !level || !grandmas) {
      clearLocalStorage();
    }

    setCookiesCounter(cookies ? Number(cookies) : 0);
    setCursors(cursors ? Number(cursors) : 0);
    setGrandmas(grandmas ? Number(grandmas) : 0);
  }, [setCookiesCounter, setCursors, setGrandmas]);

  return (
    <BrowserRouter>
      <Switch>
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route} />
        ))}
      </Switch>
    </BrowserRouter>
  );
};

const RouteWithSubRoutes = (route) => {
  return (
    <Route
      route
      path={route.path}
      render={(props) => <route.component {...props} routes={route.routes} />}
    />
  );
};

export default App;
