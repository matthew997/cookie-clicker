import React, { useEffect } from "react";
import {
  cookiesCounter,
  saveSession,
  cursorCounter,
  grandmasCounter,
} from "@state/store";
import { whatIsMyLevel } from "@state/helpers";
import { useRecoilState, useRecoilValue } from "recoil";

import hand from "@assets/images/hand.png";

const renderCursors = (n) => {
  let content = [];
  for (let i = 0; i < n; i++) {
    content.push(
      <li key={i}>
        <img src={hand} alt="img" />
      </li>
    );
  }

  return content;
};

const CookieButton = () => {
  const [cookies, setCookiesCounter] = useRecoilState(cookiesCounter);
  const cursors = useRecoilValue(cursorCounter);
  const grandmas = useRecoilValue(grandmasCounter);

  const perfectCookieButton = () => {
    const { cookiesPerClick } = whatIsMyLevel(cookies);

    const cookiesAmount = cookies + cookiesPerClick;

    setCookiesCounter(cookiesAmount);
    saveSession("cookies", cookiesAmount);
  };

  useEffect(() => {
    const intervalID = setInterval(() => {
      const value =
        cookies + 0.1 * cursors * (1000 / 100) + grandmas * (1000 / 100);
      setCookiesCounter(value);
      saveSession("cookies", value);
    }, 1000);
    return () => clearInterval(intervalID);
  }, [cookies, cursors, grandmas, setCookiesCounter]);

  return (
    <div className="clicker-center">
      <div id="shine" />
      <ul className="circle-container">{renderCursors(cursors)}</ul>
      <div id="cookie" datascontent="👋" onClick={perfectCookieButton} />
    </div>
  );
};

export default CookieButton;
