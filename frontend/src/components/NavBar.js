import { useSetRecoilState } from "recoil";
import {
  cookiesCounter,
  cursorCounter,
  grandmasCounter,
  clearLocalStorage,
} from "@state/store";

import shop from "@assets/images/shop.png";
import cookie from "@assets/images/cookie.png";
import achievements from "@assets/images/achievements.png";
import cookieMonster from "../assets/images/cookieMonster.png";
import { Link } from "react-router-dom";

const NavBar = () => {
  const setCookiesCounter = useSetRecoilState(cookiesCounter);
  const setCursors = useSetRecoilState(cursorCounter);
  const setGrandmas = useSetRecoilState(grandmasCounter);

  const clear = () => {
    clearLocalStorage();
    setCookiesCounter(0);
    setCursors(0);
    setGrandmas(0);
  };

  return (
    <div className="navbar">
      <Link to="/">
        <img src={cookie} alt="img" />
        <small>Home</small>
      </Link>

      <Link to="/shop">
        <img src={shop} alt="img" />
        <small>Shop</small>
      </Link>

      <Link to="/achievements">
        <img src={achievements} alt="img" />
        <small>Achievements</small>
      </Link>

      <div onClick={clear}>
        <img src={cookieMonster} alt="img" />
        <small>...</small>
      </div>
    </div>
  );
};

export default NavBar;
