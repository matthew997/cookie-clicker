import { levels, whatIsMyLevelIndex } from "@state/helpers";

const ListElement = (props) => {
  const { index, item, className, achieved } = props;

  return (
    <li className={className}>
      <div>Level {index + 1}</div>
      <div>
        {item.name} -
        {achieved
          ? ` osiągnięto ${item.cookies - 1} ciastek`
          : ` wymagene ${item.cookies - 1} 🍪  - Dostępne wkrótce :)`}
      </div>
    </li>
  );
};

const PlayerAchievements = () => {
  const level = localStorage.level ? localStorage.level : 0;
  const levelIndex = whatIsMyLevelIndex(level);

  const listItems = levels.map((item, index) => {
    if (levelIndex > index) {
      return (
        <ListElement
          className="green"
          index={index}
          item={item}
          key={index}
          achieved={true}
        />
      );
    } else if (levelIndex >= index) {
      return (
        <ListElement
          className=""
          index={index}
          item={item}
          achieved={false}
          key={index}
        />
      );
    } else {
      return null;
    }
  });

  return (
    <div>
      <ul className="level-list">{listItems}</ul>
    </div>
  );
};

export default PlayerAchievements;
