import {
  cookiesCounter,
  cursorCounter,
  grandmasCounter,
  saveSession,
} from "@state/store";
import { defaultPrices, whatIsMyLevel } from "@state/helpers";
import { useRecoilState, useRecoilValue } from "recoil";

import grandmaImage from "../assets/images/grandma.png";
import cursorImage from "../assets/images/cursor.png";

const Product = (props) => {
  const { src, productName, priceRatio, defaultPrices, quantity, set } = props;
  const [cookies, setCookiesCounter] = useRecoilState(cookiesCounter);

  const price = () => {
    if (quantity) {
      return Math.round(defaultPrices * quantity * priceRatio);
    } else {
      return Math.round(defaultPrices * priceRatio);
    }
  };

  const isActive = () => {
    return cookies > price();
  };

  const buyCursor = () => {
    if (isActive()) {
      const cookiesAfter = cookies - price();

      setCookiesCounter(cookiesAfter);
      set(quantity + 1);
      saveSession("cookies", cookiesAfter);
      saveSession(productName, quantity + 1);
    }
  };

  return (
    <li onClick={buyCursor} className={isActive() ? "active" : ""}>
      <img src={src} alt={productName} />
      <div>
        <div className={`product-title ${isActive() ? "active" : ""}`}>
          {productName}
        </div>
        <div className={`product-price ${isActive() ? "active" : ""}`}>
          -{price()} 🍪
        </div>
      </div>
      <div className="quantity">{quantity}</div>
    </li>
  );
};

function ShopItems() {
  const cookies = useRecoilValue(cookiesCounter);
  const [cursors, setCursorCounter] = useRecoilState(cursorCounter);
  const [grandma, setGrandmasCounter] = useRecoilState(grandmasCounter);
  const { priceRatio } = whatIsMyLevel(cookies);
  const { cursorsPrice, grandmasPrice } = defaultPrices;

  return (
    <div>
      <ul className="shop-list">
        <Product
          src={cursorImage}
          productName="cursors"
          priceRatio={priceRatio}
          defaultPrices={cursorsPrice}
          quantity={cursors}
          set={setCursorCounter}
        />
        <Product
          src={grandmaImage}
          productName="grandmas"
          priceRatio={priceRatio}
          defaultPrices={grandmasPrice}
          quantity={grandma}
          set={setGrandmasCounter}
        />
      </ul>
    </div>
  );
}

export default ShopItems;
