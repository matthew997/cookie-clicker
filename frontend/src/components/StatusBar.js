import { useRecoilValue } from "recoil";
import { cookiesCounter } from "@state/store";
import { whatIsMyLevel } from "@state/helpers";

export const CookiesGoingDown = () => {
  return (
    <div className="falling-down-cookies-container">
      <div className="cookies-snow cookies-snow--near" />
      <div className="cookies-snow cookies-snow--near cookies-snow--alt" />
    </div>
  );
};

const StatusBar = () => {
  const cookies = useRecoilValue(cookiesCounter);
  const myLevel = whatIsMyLevel(cookies);

  return (
    <div>
      {cookies > 10 ? <CookiesGoingDown /> : ""}
      <div className="status-bar">
        <div className="title">Cookies: {cookies} </div>
        <small>cookies per click: {myLevel.cookiesPerClick}</small>
      </div>
    </div>
  );
};

export default StatusBar;
