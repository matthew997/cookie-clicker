import PageNotFound from "@views/PageNotFound";
import Home from "@views/Home";
import Achievements from "@views/Achievements";
import Shop from "@views/Shop";

const routes = [
  {
    path: "/",
    component: Home,
    exact: true,
  },
  {
    path: "/achievements",
    component: Achievements,
    exact: true,
  },
  {
    path: "/shop",
    component: Shop,
    exact: true,
  },
  {
    path: "/**",
    component: PageNotFound,
    exact: false,
  },
];

export default routes;
