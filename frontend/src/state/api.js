import axios from "@config/axios";

const progress = async (params) => {
  const { data } = await axios.patch("progress", params);

  return data;
};

export { progress };
