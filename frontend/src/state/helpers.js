const levels = [
  {
    level: 1,
    name: "Dopiero zaczynasz",
    cookies: 10,
    cookiesPerClick: 1,
    priceRatio: 1,
  },
  {
    level: 2,
    name: "Całkiem nieźle",
    cookies: 20,
    cookiesPerClick: 2,
    priceRatio: 1.1,
  },
  {
    level: 3,
    name: "Oby tak dalej",
    cookies: 40,
    cookiesPerClick: 3,
    priceRatio: 1.2,
  },
  {
    level: 4,
    name: "Tak trzymaj 👍",
    cookies: 50,
    cookiesPerClick: 4,
    priceRatio: 1.3,
  },
  {
    level: 5,
    name: "Jesteś już blisko 💯",
    cookies: 90,
    cookiesPerClick: 5,
    priceRatio: 1.4,
  },
  {
    level: 6,
    name: "500+",
    cookies: 500,
    cookiesPerClick: 6,
    priceRatio: 1.5,
  },
  {
    level: 7,
    name: "Ciasteczkowy potwór 🍪 👻",
    cookies: 50000,
    cookiesPerClick: 7,
    priceRatio: 2,
  },
  {
    level: 8,
    name: "Mamy to... ",
    cookies: 100000,
    cookiesPerClick: 8,
    priceRatio: 3,
  },
  {
    level: 9,
    name: "Jesteś the best 🥉",
    cookies: 250000,
    cookiesPerClick: 10,
    priceRatio: 4,
  },

  {
    level: 10,
    name: "🥈",
    cookies: 1000000,
    cookiesPerClick: 11,
    priceRatio: 4.5,
  },
  {
    level: 11,
    name: "🥇",
    cookies: 10000000,
    cookiesPerClick: 15,
    priceRatio: 5,
  },
];

const defaultPrices = {
  cursorsPrice: 10,
  grandmasPrice: 100,
};

const whatIsMyLevel = (cookies) => {
  const { level: storedLevel } = localStorage;
  const currentLevel = levels.find((level) => level.cookies > cookies);

  if (currentLevel === undefined || !storedLevel) {
    return levels[0];
  }

  if (currentLevel.level >= Number(storedLevel)) {
    localStorage.setItem("level", currentLevel.level);
    return currentLevel;
  } else {
    return levels[storedLevel];
  }
};

const whatIsMyLevelIndex = (level) => {
  return levels.findIndex((item) => item.level === Number(level));
};

export { levels, whatIsMyLevel, whatIsMyLevelIndex, defaultPrices };
