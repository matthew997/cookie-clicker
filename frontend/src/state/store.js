import { atom } from "recoil";

const cookiesCounter = atom({
  key: "cookiesCounter",
  default: 0,
});

const cursorCounter = atom({
  key: "cursorCounter",
  default: 0,
});

const grandmasCounter = atom({
  key: "grandmasCounter",
  default: 0,
});

const saveSession = (key, value) => {
  localStorage.setItem(key, value);
};

const clearLocalStorage = () => {
  saveSession("cursors", 0);
  saveSession("grandmas", 0);
  saveSession("level", 0);
  saveSession("cookies", 0);
};

export {
  cookiesCounter,
  cursorCounter,
  grandmasCounter,
  saveSession,
  clearLocalStorage,
};
