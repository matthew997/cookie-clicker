import NavBar from "@components/NavBar";
import StatusBar from "@components/StatusBar";
import PlayerAchievements from "@components/PlayerAchievements";

const Achievements = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <div className="game">
          <StatusBar />
          <PlayerAchievements />
        </div>
        <NavBar />
      </div>
    </div>
  );
};

export default Achievements;
