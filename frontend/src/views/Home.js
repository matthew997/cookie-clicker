import CookieButton from "@components/CookieButton";
import NavBar from "@components/NavBar";
import StatusBar from "@components/StatusBar";

const Home = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <div className="game">
          <StatusBar />
          <CookieButton />
        </div>
        <NavBar />
      </div>
    </div>
  );
};

export default Home;
