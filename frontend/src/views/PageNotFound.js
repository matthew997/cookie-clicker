import NavBar from "@components/NavBar";

function PageNotFound() {
  return (
    <div className="container">
      <div className="wrapper">
        <div className="box-error">
          <h1>404</h1>
        </div>
        <NavBar />
      </div>
    </div>
  );
}

export default PageNotFound;
