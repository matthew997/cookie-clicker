import NavBar from "@components/NavBar";
import StatusBar from "@components/StatusBar";
import ShopItems from "@components/ShopItems";

const Shop = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <div className="game">
          <StatusBar />
          <ShopItems />
        </div>
        <NavBar />
      </div>
    </div>
  );
};

export default Shop;
